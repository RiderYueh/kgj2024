using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class CustomerManager : MonoBehaviour
{
    public static CustomerManager instance;
    private void Awake()
    {
        instance = this;
    }

    public List<GameObject> list_whiteMan;
    public List<GameObject> list_tatooMan;
    public List<GameObject> list_blueWoman;

    public GameObject go_CustomerParents;



    private Animator cusAnimator;
    public void OnCustomer()
    {
        CreateCustomer();
        OnStartCustomerWalk();
    }

    private void CreateCustomer()
    {
        LevelData ld = GameFlowManager.Instancce.GetLevelData;
        int tempX = 0;
        foreach (int i in ld.Levels[GameBasicData.Instance.GetDataDay].GuestType)
        {
            if (i == 0)
            {
                GameObject go = Instantiate(list_whiteMan[0], go_CustomerParents.transform) as GameObject;
                go.transform.position = go.transform.parent.position - new Vector3(tempX , 0 , 0);
                tempX += 5;

            }
            else if(i == 1)
            {
                GameObject go = Instantiate(list_tatooMan[0], go_CustomerParents.transform) as GameObject;
                go.transform.position = go.transform.parent.position - new Vector3(tempX, 0, 0);
                tempX += 5;

            }
            else if (i == 2)
            {
                GameObject go = Instantiate(list_blueWoman[0], go_CustomerParents.transform) as GameObject;
                go.transform.position = go.transform.parent.position - new Vector3(tempX, 0, 0);
                tempX += 5;

            }
        }
    }

    List<Animator> list_customerAnimator;
    private void OnStartCustomerWalk()
    {
        list_customerAnimator = new List<Animator>();
        foreach (Transform child in go_CustomerParents.transform)
        {
            list_customerAnimator.Add(child.GetComponent<Animator>());
        }

        foreach(Animator am in list_customerAnimator)
        {
            am.Play("Walk");
        }
    }

    bool isSelling = false;
    int customerCount = 0;
    private void Update()
    {
        if(!isSelling)
        {
            for (int i = 0; i < list_customerAnimator.Count; i++)
            {
                if(customerCount > i)
                {
                    list_customerAnimator[i].Play("Run");
                    list_customerAnimator[i].transform.position += new Vector3(Time.deltaTime * 30, 0, 0);
                    if (list_customerAnimator[list_customerAnimator.Count - 1].transform.position.x > 50)
                    {
                        //結束
                    }
                }
                else
                {
                    list_customerAnimator[i].Play("Walk");
                    list_customerAnimator[i].transform.position += new Vector3(Time.deltaTime * 4, 0, 0);
                    if (list_customerAnimator[i].transform.position.x >= 0)
                    {
                        isSelling = true;
                        OnCustomerWaitSell();
                        customerCount += 1;
                        GameFlowManager.Instancce.OnCountFace();
                        GameFlowManager.Instancce.OnCountHunger();
                        list_customerAnimator[i].transform.DOMoveY(-1.9f, 0.5f);
                        list_customerAnimator[i].transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.5f);
                    }
                }
            }
        }

        if(Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown("p"))
            {
                GameFlowManager.Instancce.NextLevel();
            }

            if (Input.GetKeyDown("="))
            {
                Time.timeScale += 1;
            }
            if (Input.GetKeyDown("-"))
            {
                Time.timeScale = 1;
            }

            if (Input.GetKeyDown("m"))
            {
                GameFlowManager.Instancce.GetCurrentGuest.Satisfaction += 1;
                GameFlowManager.Instancce.OnChangeMoney(1000);
            }
        }

    }

    public bool IsCanLeave()
    {
        if (!isSelling) return false;
        return true;
    }

    public bool IsCanUseItem()
    {
        if (!isSelling) return false;
        if(GameFlowManager.Instancce.GetCurrentGuest.isFull) return false;

        return true;
    }

    public void OnCustomerWaitSell()
    {
        foreach (Animator am in list_customerAnimator)
        {
            am.Play("Idle");
        }
    }

    public void OnLeave()
    {
        isSelling = false;
    }

    private IEnumerator WaitTimeAction(float timer, UnityAction ua)
    {
        yield return new WaitForSeconds(timer);
        ua();
    }

    public void OnReset()
    {
        isSelling = false;
        customerCount = 0;
        foreach (Transform child in go_CustomerParents.transform)
        {
            Destroy(child.gameObject);
        }
        list_customerAnimator.Clear();
        FoodUI.instance.OnReset();
    }
}
