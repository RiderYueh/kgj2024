﻿using UnityEngine;

public class Launcher : MonoBehaviour
{
    [SerializeField] private LevelData levelData;
    [SerializeField] private GuestTypeData guestTypeData;
    [SerializeField] private PriceData priceData;
    [SerializeField] private OutsideMono monoPlus;

    private void Start()
    {
        GameBasicData.Instance.ResetData();
        monoPlus.CardDeck = CardTest.instance;
        monoPlus.FoodUI = FoodUI.instance;
        monoPlus.StoreCtrl.InitFixCards();
        GameFlowManager.Instancce.InitMono(monoPlus);
        GameFlowManager.Instancce.InitData(levelData, guestTypeData, priceData);
        GameFlowManager.Instancce.InitGuestList(GameBasicData.Instance.GetDataDay);
        CardTest.instance.OnStart();
        GameFlowManager.Instancce.StartSell();
    }
}