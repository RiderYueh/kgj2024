﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="New PriceData",menuName="ScriptableObject/PriceData")]
public class PriceData : ScriptableObject {

    public List<CardTypeData> CardPriceList;
    
    [Space(30)]
    /// <summary>
    /// 非常滿意的倍數
    /// </summary>
    public float VSatisfactionMulti = 1.5f;

    /// <summary>
    /// 滿意的倍數
    /// </summary>
    public float SatisfactionMulti = 1.2f;

    /// <summary>
    /// 普通的倍數
    /// </summary>
    public float NormalMulti = 1.0f;

    /// <summary>
    /// 不滿意的倍數
    /// </summary>
    public float DissatisfactionMtuli = 0.4f;

    /// <summary>
    /// 非常不滿意的倍數
    /// </summary>
    public float VDissatisfactionMulti = 0.1f;
}