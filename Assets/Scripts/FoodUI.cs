using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using CliffLeeCL;

public class FoodUI : MonoBehaviour
{
    public static FoodUI instance;
    private void Awake()
    {
        instance = this;
    }

    public GameObject go_foodPrefab;
    public Image img_faceIcon;

    public List<GameObject> list_HungerEmptyGO;
    public List<GameObject> list_HungerFullGO;

    private int maxFood = 3;

    public void OnSetImage()
    {
        for (int i = transform.childCount; i < maxFood; i++)
        {
            GameObject go = Instantiate(go_foodPrefab, transform) as GameObject;
            go.SetActive(true);

            Food fd = go.GetComponent<Food>();
            CardData cd = CardTest.instance.MainCard.OnDrawCard();
            fd.MainImage.sprite = SpriteManager.Instance.GetSprite(cd.cardType);
            fd.Init(cd.cardID , cd.cardType);
        }

        int posx = -350;
        foreach (Transform child in transform)
        {
            Food fd = child.GetComponent<Food>();
            fd.MainImage.rectTransform.anchoredPosition = new Vector2(posx, -397);
            posx += 350;
        }
    }

    public void OnClickFood(int cardID)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Food fd = transform.GetChild(i).GetComponent<Food>();
            if (fd.CardID == cardID)
            {
                Destroy(fd.gameObject);
                StartCoroutine(WaitTimeAction(0.2f , ()=> { OnSetImage(); }));
                
                return;
            }
        }

    }

    public void OnReset()
    {
        foreach(Transform child in transform)
        {
            Destroy(child.gameObject);
        }
        OnRestHunger();

    }

    public void OnFull()
    {
        StartCoroutine(WaitTimeAction(1f, () => { OnBtnLeave(); }));
    }

    public void OnBtnLeave()
    {
        if (!CustomerManager.instance.IsCanLeave()) return;
        OnCloseFace();
        CustomerManager.instance.OnLeave();
        GameFlowManager.Instancce.GuestLeave();
        AudioManager.Instance.PlaySound(AudioManager.AudioName.MoneyEarned);
    }

    public void OnShowFace(int faceID)
    {
        img_faceIcon.gameObject.SetActive(true);
        img_faceIcon.sprite = SpriteManager.Instance.list_faceSprites[faceID];
    }

    public void OnCloseFace()
    {
        img_faceIcon.gameObject.SetActive(false);
    }

    private IEnumerator WaitTimeAction(float timer, UnityAction ua)
    {
        yield return new WaitForSeconds(timer);
        ua();
    }

    private void OnRestHunger()
    {
        OnResetHungerEmpty();
        OnResetHungreFull();
    }

    private void OnResetHungerEmpty()
    {
        foreach (GameObject go in list_HungerEmptyGO)
        {
            go.SetActive(false);
        }
    }

    private void OnResetHungreFull()
    {
        foreach (GameObject go in list_HungerFullGO)
        {
            go.SetActive(false);
        }
    }

    public void OnShowHunger(int hunger , int full )
    {
        OnRestHunger();
        for (int i = 0; i < hunger; i++)
        {
            list_HungerFullGO[i].SetActive(true);
        }

        for (int i = 0; i < full; i++)
        {
            list_HungerEmptyGO[i].SetActive(true);
        }
    }

}
