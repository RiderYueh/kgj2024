﻿using System.Collections.Generic;

public class GameBasicData
{

    private static GameBasicData instance;

    public static GameBasicData Instance
    {
        get
        {
            if(instance == null)
                instance = new GameBasicData();

            return instance;
        }
    }

    public int Day = 1;
    public int Rating = 50;
    public int Money = 0;

    public List<int> CardUsedAmountList = new();

    public int GetDataDay
    {
        get
        {
            return Day/2;
        }
    }

    public void ResetData()
    {
        Day = 1;
        Rating = 50;
        Money = 250;
    }
}