using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using CliffLeeCL;

public class GameFlowManager
{
    private static GameFlowManager instance = null;
    public static GameFlowManager Instancce
    {
        get
        {
            if(instance == null)
                instance = new GameFlowManager();

            return instance;
        }
    }

    private OutsideMono mono;
    private GuestInfo currentGuest = null;
    public GuestInfo GetCurrentGuest
    {
        get
        {
            return currentGuest;
        }
    }

    #region Data

    private int remainGuestAmount = 3;
    private int currentGuestOrder = -1;
    public int GetCurrentGuestOrder
    {
        get
        {
            return currentGuestOrder;
        }
    }

    private LevelData levelData = null;
    private GuestTypeData guestTypeData = null;
    private PriceData priceData = null;

    public LevelData GetLevelData
    {
        get
        {
            return levelData;
        }
    }

    public GuestTypeData GetGuestTypeData
    {
        get
        {
            return guestTypeData;
        }
    }

    public PriceData GetPriceData
    {
        get
        {
            return priceData;
        }
    }

    #endregion

    public void InitMono(OutsideMono outsideMono)
    {
        mono = outsideMono;
    }

    public void InitData(LevelData level, GuestTypeData guestType, PriceData price)
    {
        levelData = level;
        guestTypeData = guestType;
        priceData = price;

    }

    /// <summary>
    /// 開始營業
    /// </summary>
    public void StartSell()
    {
        AudioManager.Instance.PlayMusic(AudioManager.AudioName.MainGameBGM , 0.3f);
        InitItemAmountData();
        UpdateScoreUI();
        remainGuestAmount = levelData.Levels[GameBasicData.Instance.GetDataDay].GuestAmount;
        currentGuestOrder = -1;
        NextGuest();
        mono.CardDeck.OnStartGame();
        mono.FoodUI.OnSetImage();
        CustomerManager.instance.OnCustomer();
        mono.ScoreUI.DayText.text = GameBasicData.Instance.Day.ToString();
        mono.ScoreUI.TargetText.text = $"${levelData.Levels[GameBasicData.Instance.GetDataDay].TargetMoney:N0}";
    }

    /// <summary>
    /// 購買卡片
    /// </summary>
    public void StartBuy()
    {
        AudioManager.Instance.PlayMusic(AudioManager.AudioName.InterfaceOpenBGM, 0.5f);
        InitGuestList(GameBasicData.Instance.GetDataDay);
        mono.StoreCtrl.Open();
    }

    /// <summary>
    /// 使用卡片
    /// </summary>
    public void UseCard(int cardID)
    {
        GameBasicData.Instance.CardUsedAmountList[cardID]++;
        DetermineRating(cardID);
    }

    public void OnCountFace()
    {
        int sati = currentGuest.Satisfaction;
        if(sati <= -2)
        {
            sati = 1;
            AudioManager.Instance.PlaySound(AudioManager.AudioName.face1);
        }

        else if(sati == -1)
        {
            sati = 2;
            AudioManager.Instance.PlaySound(AudioManager.AudioName.face2);
        }
        else if(sati == 0)
        {
            sati = 3;
            AudioManager.Instance.PlaySound(AudioManager.AudioName.face3);
        }
        else if(sati == 1)
        {
            sati = 4;
            AudioManager.Instance.PlaySound(AudioManager.AudioName.face4);
        }
        else if(sati >= 2)
        {
            sati = 5;
            AudioManager.Instance.PlaySound(AudioManager.AudioName.face5);
        }
        FoodUI.instance.OnShowFace(sati);
    }

    public void OnCountHunger()
    {
        FoodUI.instance.OnShowHunger(currentGuest.Hunger, currentGuest.FullMax);
    }

    /// <summary>
    /// 觸發客人離開
    /// </summary>
    public void GuestLeave()
    {
        CalculateGuestResult();
        UpdateScoreUI();
        CheckGameState();
    }

    /// <summary>
    /// 下一個關卡
    /// </summary>
    public void NextLevel()
    {
        CustomerManager.instance.OnReset();

        if(GameBasicData.Instance.Money < levelData.Levels[GameBasicData.Instance.GetDataDay].TargetMoney
            || GameBasicData.Instance.Rating <= 0)
        {
            GameOver();
            return;
        }
        else if(GameBasicData.Instance.GetDataDay + 1 >= levelData.Levels.Count)
        {
            SceneManager.LoadScene(3);
            return;
        }

        GameBasicData.Instance.Day += 1;
        if(GameBasicData.Instance.Day % 2 == 0)
        {
            //購物日
            StartBuy();
        }
        else
        {
            //營業日
            StartSell();
        }
    }

    public void InitGuestList(int dataIndex)
    {
        levelData.Levels[dataIndex].EachGuestCount.Clear();
        LevelInfo data = levelData.Levels[dataIndex];
        if(data.IsUseRandRule)
        {
            List<int> guestList = new();
            int count;
            int count2;
            int remain;
            //第二關規則
            if(dataIndex == 1)
            {
                count = data.RuleList[0].IsFix ? data.RuleList[0].AmountMin : UnityEngine.Random.Range(data.RuleList[0].AmountMin, data.RuleList[0].AmountMax + 1);
                for(int i = 0; i < count; i++)
                {
                    guestList.Add(1);
                }
                count2 = data.RuleList[count].IsFix ? data.RuleList[count].AmountMin : UnityEngine.Random.Range(data.RuleList[count].AmountMin, data.RuleList[count].AmountMax + 1);
                for(int i = 0; i < count2; i++)
                {
                    guestList.Add(2);
                }
                remain = data.GuestAmount - (count + count2);
                for(int i = 0; i < remain; i++)
                {
                    guestList.Add(0);
                }
                levelData.Levels[dataIndex].EachGuestCount.Add(remain);
                levelData.Levels[dataIndex].EachGuestCount.Add(count);
                levelData.Levels[dataIndex].EachGuestCount.Add(count2);
            }
            //第三關規則
            else if(dataIndex == 2)
            {
                count = data.RuleList[0].IsFix ? data.RuleList[0].AmountMin : UnityEngine.Random.Range(data.RuleList[0].AmountMin, data.RuleList[0].AmountMax + 1);
                for(int i = 0; i < count; i++)
                {
                    guestList.Add(2);
                }
                count2 = data.RuleList[1].IsFix ? data.RuleList[1].AmountMin : UnityEngine.Random.Range(data.RuleList[1].AmountMin, data.RuleList[1].AmountMax + 1);
                for(int i = 0; i < count2; i++)
                {
                    guestList.Add(1);
                }
                remain = data.GuestAmount - (count + count2);
                for(int i = 0; i < remain; i++)
                {
                    guestList.Add(0);
                }
                levelData.Levels[dataIndex].EachGuestCount.Add(remain);
                levelData.Levels[dataIndex].EachGuestCount.Add(count2);
                levelData.Levels[dataIndex].EachGuestCount.Add(count);
            }

            List<int> guestRandOrder = new();
            for(int i = 0; i < guestList.Count; i++)
            {
                int order = UnityEngine.Random.Range(0, guestList.Count);
                while(guestList[order] == -1)
                {
                    order = UnityEngine.Random.Range(0, guestList.Count);
                }
                int guestType = guestList[order];
                guestRandOrder.Add(guestType);
                guestList[order] = -1;
            }

            levelData.Levels[dataIndex].GuestType = guestRandOrder;
        }
    }

    /// <summary>
    /// 初始化品項計數
    /// </summary>
    private void InitItemAmountData()
    {
        GameBasicData.Instance.CardUsedAmountList.Clear();
        for(int i = 0; i < priceData.CardPriceList.Count; i++)
        {
            GameBasicData.Instance.CardUsedAmountList.Add(0);
        }
    }

    /// <summary>
    /// 傳送客人動作
    /// </summary>
    private void SendGuestAction()
    {
        OnCountFace();
        OnCountHunger();

    }

    /// <summary>
    /// 評級判定
    /// </summary>
    private void DetermineRating(int cardID)
    {
        Debug.Log("吃了甚麼 : " +cardID.ToString());
        bool hasLove = currentGuest.LoveFoods.Exists(x => x == cardID);
        //bool hasHate = currentGuest.HateFoods.Exists(x => x == cardID);
        bool hasHate = false;
        if (!hasLove) hasHate = true;
        currentGuest.Hunger += 1;
        GameBasicData.Instance.Money -= priceData.CardPriceList[cardID].Cost;
        UpdateMoney();

        if(hasLove)
            currentGuest.Satisfaction += 1;
        if(hasHate)
            currentGuest.Satisfaction += -1;

        //要判斷客人是否吃特殊食物
        if(hasLove)
        {
            if (cardID == 7) //清酒 老頭 降飽食度2 
            {
                if(currentGuest.Type == 0)
                    currentGuest.Hunger -= 2;
            }
            else if (cardID == 3) //沙拉
            {
                currentGuest.Satisfaction += 1;
            }
            else if (cardID == 9) //珍奶
            {
                if(currentGuest.Type == 1) //刺青男
                {
                    currentGuest.Hunger += 1;
                    currentGuest.Satisfaction += 1;
                }
            }
        }
        

        //判斷是否提升FullMax
        if (hasLove)
        {
            if(currentGuest.Satisfaction >= 1)
            {
                if(!currentGuest.isFullAdd1)
                {
                    currentGuest.isFullAdd1 = true;
                    currentGuest.FullMax += 1;
                }
            }
            if(currentGuest.Satisfaction >= 2)
            {
                if(!currentGuest.isFullAdd2)
                {
                    currentGuest.isFullAdd2 = true;
                    currentGuest.FullMax += 1;
                }
            }
        }

        if(currentGuest.Hunger >= currentGuest.FullMax)
        {
            currentGuest.isFull = true;
            FoodUI.instance.OnFull();
        }

        if (currentGuest.Hunger <= 0) currentGuest.Hunger = 0;
        if (currentGuest.Satisfaction >= 2)
            currentGuest.Satisfaction = 2;

        SendGuestAction();
        mono.ScoreUI.OnShowPreMoney();

        if (currentGuest.Satisfaction <= -2)
        {
            //生氣就跑
            currentGuest.isFull = true;
            FoodUI.instance.OnFull();
        }    
    }

    public int CalculatePreMoney()
    {
        //===新的muti算法
        float multi;
        float fee;
        if(currentGuest.Satisfaction >= 2)
            fee = 100;
        else
            fee = 0;

        if(currentGuest.Satisfaction <= -2)
            multi = 0;
        else
            multi = 1;

        int earnedMoney = 0;
        int count = GameBasicData.Instance.CardUsedAmountList.Count;
        for(int i = 0; i < count; i++)
        {
            earnedMoney += priceData.CardPriceList[i].Money * GameBasicData.Instance.CardUsedAmountList[i];
        }
        earnedMoney = (int)(earnedMoney * multi + fee);
        if(earnedMoney == 0)
        {
            //只有離場費
            earnedMoney = 50;
        }

        return earnedMoney;
    }

    public bool IsHappy()
    {
        if (currentGuest.Satisfaction >= 2) return true;
        return false;
    }


    /// <summary>
    /// 計算分數
    /// </summary>
    private void CalculateGuestResult()
    {
        /*float multi;

        if(currentGuest.Satisfaction >= 2)
        {
            multi = priceData.VSatisfactionMulti;
            GameBasicData.Instance.Rating += 2;
        }
        else if(currentGuest.Satisfaction == 1)
        {
            multi = priceData.SatisfactionMulti;
            GameBasicData.Instance.Rating += 1;
        }
        else if(currentGuest.Satisfaction == 0)
        {
            multi = priceData.NormalMulti;
        }
        else if(currentGuest.Satisfaction == -1)
        {
            multi = priceData.DissatisfactionMtuli;
            GameBasicData.Instance.Rating -= 1;
        }
        else
        {
            multi = priceData.VDissatisfactionMulti;
            GameBasicData.Instance.Rating += currentGuest.Satisfaction;
        }
        */

        mono.ScoreUI.OnClosePreMoney();
        GameBasicData.Instance.Rating += currentGuest.Satisfaction;
        OnChangeMoney(CalculatePreMoney());

        UnityEngine.Debug.Log("GFM : MONEY  " + GameBasicData.Instance.Money);
        UnityEngine.Debug.Log("GFM : RATING  " + GameBasicData.Instance.Rating);
    }

    public void OnChangeMoney(int value)
    {
        GameBasicData.Instance.Money += value;
    }

    /// <summary>
    /// 檢查階段
    /// </summary>
    private void CheckGameState()
    {
        if(remainGuestAmount == 0)
        {
            DOVirtual.DelayedCall(3, () =>
            {
                NextLevel();
            });
        }
        else
        {
            NextGuest();
        }
    }

    /// <summary>
    /// 下一位客人
    /// </summary>
    private void NextGuest()
    {
        remainGuestAmount--;
        InitItemAmountData();
        currentGuestOrder++;
        int guestIndex = levelData.Levels[GameBasicData.Instance.GetDataDay].GuestType[currentGuestOrder];
        currentGuest = new GuestInfo();
        currentGuest.Hunger = 0;
        currentGuest.FullMax = 2;
        currentGuest.Type = guestTypeData.Guests[guestIndex].Type;
        currentGuest.LoveFoods = new(guestTypeData.Guests[guestIndex].LoveFoods);
        currentGuest.HateFoods = new(guestTypeData.Guests[guestIndex].HateFoods);
        currentGuest.isPokerFace = guestTypeData.Guests[guestIndex].isPokerFace;
        currentGuest.Satisfaction = Random.Range(-1, 2);
        currentGuest.isFullAdd1 = false;
        currentGuest.isFullAdd2 = false;
        currentGuest.isFull = false;
    }

    /// <summary>
    /// 遊戲結束
    /// </summary>
    private void GameOver()
    {
        SceneManager.LoadScene(2);
    }

    private void UpdateScoreUI()
    {
        UpdateMoney();
        SetRatingStar();
    }

    private void UpdateMoney()
    {
        mono.ScoreUI.MoneyText.text = $"${GameBasicData.Instance.Money:N0}";
    }

    private void SetRatingStar()
    {
        int ratingLv;

        if(GameBasicData.Instance.Rating >= 90)
        {
            //5
            ratingLv = 5;
        }
        else if(GameBasicData.Instance.Rating >= 70)
        {
            //4
            ratingLv = 4;
        }
        else if(GameBasicData.Instance.Rating >= 50)
        {
            //3
            ratingLv = 3;
        }
        else if(GameBasicData.Instance.Rating >= 30)
        {
            //2
            ratingLv = 2;
        }
        else if(GameBasicData.Instance.Rating >= 1)
        {
            //1
            ratingLv = 1;
        }
        else
        {
            //0
            ratingLv = 0;
        }

        mono.ScoreUI.SetStarCount(ratingLv);
    }

}