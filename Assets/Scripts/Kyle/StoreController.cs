using CliffLeeCL;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class StoreController : MonoBehaviour
{
    [SerializeField] private PriceData priceRef;
    [SerializeField] private GameObject mainUIObj;
    [SerializeField] private GameObject shoppingPage;
    [SerializeField] private Text moneyText;
    [SerializeField] private Text moneyText2;
    [SerializeField] private Text moneyNextDayText;
    [SerializeField] private List<Image> iconList;
    [SerializeField] private List<Image> iconCustomerList;
    [SerializeField] private List<Text> sellList;
    [SerializeField] private List<Text> sellCostList;
    [SerializeField] private List<Text> countList;
    [SerializeField] private List<Text> priceList;
    [SerializeField] private List<Button> btnShoppingList;
    [SerializeField] private List<Text> guestAmountList;

    private List<int> shoppingCardIDList = new();
    private List<int> shoppingCountDefineList = new() { -1, -1, -1, -1, 3, 3 };
    private List<int> shoppingCardCountList = new();

    //==============================
    public GameObject go_Adjust;
    public GameObject go_backpackCardParents;
    public StoreCardPrefab scp;

    public GameObject go_nextCustomer;
    public List<Image> nextDayCutomerImageList;

    private void Start()
    {
        shoppingPage.SetActive(false);
    }

    private void OnShowNextDay()
    {
        foreach(Image img in nextDayCutomerImageList)
        {
            img.gameObject.SetActive(false);
        }

        bool isHaveGuest1 = false;
        bool isHaveGuest2 = false;
        bool isHaveGuest3 = false;
        foreach(int GuestType in GameFlowManager.Instancce.GetLevelData.Levels[GameBasicData.Instance.GetDataDay].GuestType)
        {
            if(GuestType == 0)
                isHaveGuest1 = true;
            if(GuestType == 1)
                isHaveGuest2 = true;
            if(GuestType == 2)
                isHaveGuest3 = true;
        }

        int count = 0;
        if(isHaveGuest1)
        {
            nextDayCutomerImageList[count].gameObject.SetActive(true);
            nextDayCutomerImageList[count].sprite = SpriteManager.Instance.list_customerIcon[0];
            count++;
        }
        if(isHaveGuest2)
        {
            nextDayCutomerImageList[count].gameObject.SetActive(true);
            nextDayCutomerImageList[count].sprite = SpriteManager.Instance.list_customerIcon[1];
            count++;
        }
        if(isHaveGuest3)
        {
            nextDayCutomerImageList[count].gameObject.SetActive(true);
            nextDayCutomerImageList[count].sprite = SpriteManager.Instance.list_customerIcon[2];
            count++;
        }

        guestAmountList[0].text = "x" + GameFlowManager.Instancce.GetLevelData.Levels[GameBasicData.Instance.GetDataDay].EachGuestCount[0];
        guestAmountList[1].text = "x" + GameFlowManager.Instancce.GetLevelData.Levels[GameBasicData.Instance.GetDataDay].EachGuestCount[1];
        guestAmountList[2].text = "x" + GameFlowManager.Instancce.GetLevelData.Levels[GameBasicData.Instance.GetDataDay].EachGuestCount[2];

        moneyNextDayText.text = "隔天目標 : $" + GameFlowManager.Instancce.GetLevelData.Levels[GameBasicData.Instance.GetDataDay].TargetMoney.ToString();
    }

    public void OnRefreshMoney()
    {
        moneyText.text = GameBasicData.Instance.Money.ToString();
        moneyText2.text = GameBasicData.Instance.Money.ToString();
    }

    public void Open()
    {
        foreach(Button item in btnShoppingList)
        {
            item.interactable = true;
        }
        OnRefreshMoney();
        mainUIObj.SetActive(true);
        go_nextCustomer.SetActive(true);
        CreateCardItems();
        CheckButtonState();
        OnShowNextDay();
    }

    public void Update()
    {
        if(Input.GetKeyDown("b"))
        {
            CardTest.instance.OnAddCard(0);
            CardTest.instance.OnAddCard(1);
            CardTest.instance.OnAddCard(2);
            CardTest.instance.OnAddCard(3);
            CardTest.instance.OnAddCard(4);
            CardTest.instance.OnAddCard(5);
            CardTest.instance.OnAddCard(6);
            CardTest.instance.OnAddCard(7);
            CardTest.instance.OnAddCard(8);
            CardTest.instance.OnAddCard(9);
            CardTest.instance.OnAddCard(10);
            CardTest.instance.OnAddCard(11);
        }
    }

    public void Close()
    {
        if(CardTest.instance.MainCard.list_CardData.Count < 10)
            return;
        AudioManager.Instance.PlaySound(AudioManager.AudioName.ButtonClicked);
        mainUIObj.SetActive(false);
        shoppingPage.SetActive(false);
        go_Adjust.SetActive(false);
        GameBasicData.Instance.Day += 1;
        GameFlowManager.Instancce.StartSell();
    }

    public void BuyCard(int cardIndex, int shopIndex)
    {
        AudioManager.Instance.PlaySound(AudioManager.AudioName.ButtonClicked);
        if(GameBasicData.Instance.Money - priceRef.CardPriceList[cardIndex].Price < 0
            || (shoppingCountDefineList[shopIndex] != -1 && shoppingCardCountList[shopIndex] <= 0))
        {
            return;
        }
        GameBasicData.Instance.Money -= priceRef.CardPriceList[cardIndex].Price;
        OnRefreshMoney();
        if(shoppingCountDefineList[shopIndex] != -1)
        {
            shoppingCardCountList[shopIndex]--;
            countList[shopIndex].text = "x" + shoppingCardCountList[shopIndex];
        }
        CheckButtonState();
        Debug.Log("BuyCard : " + cardIndex.ToString());
        CardTest.instance.OnAddCard(cardIndex);
    }

    public void InitFixCards()
    {
        if(shoppingCardIDList.Count > 0)
        {
            return;
        }

        shoppingCardCountList = new(shoppingCountDefineList);

        shoppingCardIDList.Clear();
        shoppingCardIDList.Add(5);
        shoppingCardIDList.Add(8);
        shoppingCardIDList.Add(2);
        shoppingCardIDList.Add(1);
        shoppingCardIDList.Add(-1);
        shoppingCardIDList.Add(-1);

        for(int i = 0; i < shoppingCountDefineList.Count; i++)
        {
            int cardId = shoppingCardIDList[i];
            if(cardId != -1)
            {
                sellList[i].text = "$" + priceRef.CardPriceList[cardId].Money;
                sellCostList[i].text = "$" + priceRef.CardPriceList[cardId].Cost;
                countList[i].text = "∞";
                priceList[i].text = "$" + priceRef.CardPriceList[cardId].Price;
            }
            int shoppingIndex = i;
            btnShoppingList[i].onClick.RemoveAllListeners();
            btnShoppingList[i].onClick.AddListener(() =>
            {
                BuyCard(shoppingCardIDList[shoppingIndex], shoppingIndex);
            });
        }
    }

    private void CreateCardItems()
    {
        shoppingCardCountList = new(shoppingCountDefineList);
        for(int i = 0; i < shoppingCountDefineList.Count; i++)
        {
            int value = shoppingCountDefineList[i];
            shoppingCardCountList[i] = value;
        }

        //3 7 9
        List<int> sellItemIDs = new()
        {
            3,
            7,
            9
        };
        int toRemove = Random.Range(0, sellItemIDs.Count);
        sellItemIDs.RemoveAt(toRemove);
        shoppingCardIDList[4] = sellItemIDs[0];
        shoppingCardIDList[5] = sellItemIDs[1];

        int sellIndex = 0;
        for(int i = 4; i < 6; i++)
        {
            int itemID = sellItemIDs[sellIndex];
            sellList[i].text = "$" + priceRef.CardPriceList[itemID].Money;
            sellCostList[i].text = "$" + priceRef.CardPriceList[itemID].Cost;
            countList[i].text = "x3";
            priceList[i].text = "$" + priceRef.CardPriceList[itemID].Price;
            iconList[i].sprite = SpriteManager.Instance.GetSprite(itemID);
            if(i >= 4)
            {
                int count = 0;
                if(i == 5)
                    count = 2;

                if(itemID == 7)
                {
                    iconCustomerList[count].sprite = SpriteManager.Instance.list_customerIcon[0];
                    iconCustomerList[count + 1].sprite = SpriteManager.Instance.list_customerIcon[1];
                }

                if(itemID == 3)
                {
                    iconCustomerList[count].sprite = SpriteManager.Instance.list_customerIcon[2];
                    iconCustomerList[count + 1].sprite = SpriteManager.Instance.list_customerIcon[0];
                }

                if(itemID == 9)
                {
                    iconCustomerList[count].sprite = SpriteManager.Instance.list_customerIcon[1];
                    iconCustomerList[count + 1].sprite = SpriteManager.Instance.list_customerIcon[2];
                }

            }
            sellIndex++;
        }
    }

    public void OnBtnBackPack()
    {
        AudioManager.Instance.PlaySound(AudioManager.AudioName.ButtonClicked);
        go_Adjust.SetActive(true);
        OnRestBackPack();
    }

    public void OnCreateBackPack()
    {
        foreach(CardData cd in CardTest.instance.MainCard.list_CardData)
        {
            GameObject go = Instantiate(scp.gameObject, go_backpackCardParents.transform) as GameObject;
            go.SetActive(true);
            StoreCardPrefab tempSCP = go.GetComponent<StoreCardPrefab>();
            tempSCP.storeController = this;
            tempSCP.mainImage.sprite = SpriteManager.Instance.GetSprite(cd.cardType);
            tempSCP.CardID = cd.cardID;

        }
    }

    public void OnRestBackPack()
    {
        foreach(Transform child in go_backpackCardParents.transform)
        {
            Destroy(child.gameObject);
        }
        OnCreateBackPack();
    }

    public void OnBuyCardShop()
    {
        AudioManager.Instance.PlaySound(AudioManager.AudioName.ButtonClicked);
        go_nextCustomer.SetActive(false);
    }

    public void OnBuyCardShopClose()
    {
        AudioManager.Instance.PlaySound(AudioManager.AudioName.ButtonClicked);
        go_nextCustomer.SetActive(true);
    }

    public void OnBtnSound()
    {
        AudioManager.Instance.PlaySound(AudioManager.AudioName.ButtonClicked);
    }

    private void CheckButtonState()
    {
        for(int i = 0; i < shoppingCardIDList.Count; i++)
        {
            if(priceRef.CardPriceList[shoppingCardIDList[i]].Price > GameBasicData.Instance.Money)
            {
                btnShoppingList[i].interactable = false;
            }
            if(shoppingCountDefineList[i] != -1 && shoppingCardCountList[i] <= 0)
            {
                btnShoppingList[i].interactable = false;
            }
        }
    }
}