using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using CliffLeeCL;

public class StoreCardPrefab : MonoBehaviour
{
    public Image mainImage;
    public int CardID;
    public StoreController storeController;

    public void OnBtnDel()
    {
        if (CardTest.instance.MainCard.list_CardData.Count <= 10) return;
        if (GameBasicData.Instance.Money < 50) return;

        AudioManager.Instance.PlaySound(AudioManager.AudioName.ButtonClicked);

        CardTest.instance.OnRemoveCard(CardID);
        storeController.OnRestBackPack();
        GameFlowManager.Instancce.OnChangeMoney(-50);
        storeController.OnRefreshMoney();
    }
}
