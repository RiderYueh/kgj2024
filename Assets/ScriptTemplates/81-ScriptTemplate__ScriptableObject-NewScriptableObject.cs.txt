using UnityEngine;

[CreateAssetMenu(fileName="New #SCRIPTNAME#",menuName="ScriptableObject/#SCRIPTNAME#")]
public class #SCRIPTNAME# : ScriptableObject {
    #NOTRIM#
}