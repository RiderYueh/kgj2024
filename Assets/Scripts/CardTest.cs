using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardTest : MonoBehaviour
{
    public static CardTest instance;
    public Card MainCard;
    int cardIDNow = 0;

    private void Awake()
    {
        instance = this;
    }

    public void OnStart()
    {
        CreateCardDeck();
    }

    public void OnStartGame()
    {
        MainCard.OnCreateGameDeck();
        MainCard.OnWashCard();
    }

    public void OnAddCard(int cardType)
    {
        if (PlayerPrefs.HasKey("cardIDNow"))
        {
            cardIDNow = PlayerPrefs.GetInt("cardIDNow");
        }

        MainCard.OnAddCard(cardIDNow, cardType);
        cardIDNow += 1;
        PlayerPrefs.SetInt("cardIDNow", cardIDNow);
    }

    public void OnRemoveCard(int cardID)
    {
        MainCard.OnRemoveCard(cardID);
    }


    void CreateCardDeck()
    {
        Debug.Log("CreateCardDeck");
        MainCard = new Card();
        MainCard.list_CardData = new List<CardData>();
        
        if(PlayerPrefs.HasKey("cardIDNow"))
        {
            cardIDNow = PlayerPrefs.GetInt("cardIDNow");
        }


        for (int i = 0; i < 5; i++)
        {
            OnAddCard(2);
        }
        for (int i = 0; i < 3; i++)
        {
            OnAddCard(5);
            OnAddCard(8);
        }
        OnAddCard(1);

    }

   
}

public class Card
{
    public List<CardData> list_CardData;
    public List<CardData> list_tempCardData; //遊戲使用牌組
    public List<CardData> list_tempDiscardCardData; //遊戲打掉的牌`
    public List<CardData> list_tempHandCardData; //抽到的手牌

    //產生遊戲主要牌組 開始遊戲要創一次
    public void OnCreateGameDeck()
    {
        list_tempCardData = new List<CardData>();
        list_tempDiscardCardData = new List<CardData>();
        list_tempHandCardData = new List<CardData>();
        foreach (CardData cd in list_CardData)
        {
            list_tempCardData.Add(cd);
        }
    }

    //遊戲中 抽牌一張
    public CardData OnDrawCard()
    {
        if (list_tempCardData.Count <= 0) OnWashCard();

        for (int i = 0; i < list_tempCardData.Count; i++)
        {
            list_tempHandCardData.Add(list_tempCardData[i]);
            list_tempCardData.Remove(list_tempCardData[i]);
            Debug.Log("抽到" + list_tempHandCardData[list_tempHandCardData.Count - 1].cardType.ToString());
            return list_tempHandCardData[list_tempHandCardData.Count -1];
        }

        Debug.Log("有bug");
        return null;
    }

    //遊戲中 打出一張牌
    public void OnPlayCardByID(int cardID)
    {
        for (int i = 0; i < list_tempHandCardData.Count; i++)
        {
            if (list_tempHandCardData[i].cardID == cardID)
            {
                list_tempDiscardCardData.Add(list_tempHandCardData[i]);
                list_tempHandCardData.Remove(list_tempHandCardData[i]);
            }
        }
    }

    //遊戲中 洗牌
    public void OnWashCard()
    {
        if (list_tempDiscardCardData == null) return;

        for (int i = 0; i < list_tempDiscardCardData.Count; i++)
        {
            list_tempCardData.Add(list_tempDiscardCardData[i]);
            list_tempDiscardCardData.Remove(list_tempDiscardCardData[i]);
        }

        // fisher–yates shuffle     
        for (int i = 0; i < list_tempCardData.Count; i++)
        {

            // Pick random Element
            int j = Random.Range(i, list_tempCardData.Count);

            // Swap Elements
            CardData tmp = list_tempCardData[i];
            list_tempCardData[i] = list_tempCardData[j];
            list_tempCardData[j] = tmp;
        }
    }

    //增加牌到牌庫 遊戲外功能
    public void OnAddCard(int cardID , int cardType)
    {
        CardData tempCardData = new CardData();
        tempCardData.cardID = cardID;
        tempCardData.cardType = cardType;
        list_CardData.Add(tempCardData);
    }

    //從牌庫砍掉一張牌 遊戲外功能
    public void OnRemoveCard(int cardID)
    {
        for (int i = 0; i < list_CardData.Count; i++)
        {
            if(list_CardData[i].cardID == cardID)
            {
                list_CardData.Remove(list_CardData[i]);
            }
        }
    }

    //從牌庫砍掉一張牌 (透過type)
    public void OnRemoveCardByType(int cardType)
    {
        for (int i = 0; i < list_CardData.Count; i++)
        {
            if (list_CardData[i].cardType == cardType)
            {
                list_CardData.Remove(list_CardData[i]);
                return;
            }
        }
    }

    //印出牌庫
    public void OnLogCard(List<CardData> list)
    {
        foreach (CardData cd in list)
        {
            Debug.Log(cd.cardType);
        }
    }

    
}


//卡片狀態 牌組中 手牌 正在打出 棄牌堆
public enum CardState
{
    Deck,
    Hand,
    Playing,
    Discard
}


public class CardData 
{
    public int cardID; //UniKey
    public int cardType; //對照表格ID
    public CardState cardState = CardState.Deck;

}
