﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameResultUI : MonoBehaviour
{
    [SerializeField] private Text resultDay;
    [SerializeField] private Text resultMoney;
    [SerializeField] private List<Image> Star;

    private void Start()
    {
        resultDay.text = GameBasicData.Instance.Day.ToString();
        resultMoney.text = GameBasicData.Instance.Money.ToString();

        int ratingLv;
        if(GameBasicData.Instance.Rating >= 90)
        {
            //5
            ratingLv = 5;
        }
        else if(GameBasicData.Instance.Rating >= 70)
        {
            //4
            ratingLv = 4;
        }
        else if(GameBasicData.Instance.Rating >= 50)
        {
            //3
            ratingLv = 3;
        }
        else if(GameBasicData.Instance.Rating >= 30)
        {
            //2
            ratingLv = 2;
        }
        else if(GameBasicData.Instance.Rating >= 1)
        {
            //1
            ratingLv = 1;
        }
        else
        {
            //0
            ratingLv = 0;
        }

        for(int i = 0; i < Star.Count; i++)
        {
            Star[i].enabled = false;
        }
        for(int i = 0; i < ratingLv; i++)
        {
            Star[i].enabled = true;
        }
    }
}
