﻿using System.Collections.Generic;

[System.Serializable]
public class GuestInfo {
    public int Type;
    public int Hunger; //固定0  0是最餓 吃食物慢慢上升
    public int FullMax; //固定2
    public int Satisfaction;
    public List<int> LoveFoods = new();
    public List<int> HateFoods = new();
    public bool isPokerFace;
    public bool isFullAdd1 = false;
    public bool isFullAdd2 = false;
    public bool isFull = false;
}