﻿[System.Serializable]
public class CardTypeData {
    public int TempID;
    /// <summary>
    /// 卡片名稱
    /// </summary>
    public string Name;

    /// <summary>
    /// 賣給客人可獲得的錢
    /// </summary>
    public int Money;

    /// <summary>
    /// 從商店購買的花費
    /// </summary>
    public int Price;

    /// <summary>
    /// 打卡的費用
    /// </summary>
    public int Cost;

}