﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour
{
    public Text DayText;
    public Text TargetText;
    public Text MoneyText;

    public List<Image> Star;

    public Text preMoneyText;

    public void SetStarCount(int ratingLv)
    {
        for(int i = 0; i < Star.Count; i++)
        {
            Star[i].enabled = false;
        }
        for(int i = 0; i < ratingLv; i++)
        {
            Star[i].enabled = true;
        }
    }

    public void OnShowPreMoney()
    {
        preMoneyText.gameObject.SetActive(true);
        if(GameFlowManager.Instancce.IsHappy())
        {
            int temp = GameFlowManager.Instancce.CalculatePreMoney() - 100;
            preMoneyText.text = temp.ToString() + "\n" + "+100高潮小費";
        }
        else
        {
            preMoneyText.text = GameFlowManager.Instancce.CalculatePreMoney().ToString();
        }
        
    }

    public void OnClosePreMoney()
    {
        preMoneyText.gameObject.SetActive(false);
    }
}