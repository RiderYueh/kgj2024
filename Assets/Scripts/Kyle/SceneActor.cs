using UnityEngine;
using UnityEngine.SceneManagement;
using CliffLeeCL;

public class SceneActor : MonoBehaviour
{

    private void Start()
    {
        AudioManager.Instance.PlayMusic(AudioManager.AudioName.MainMenuBGM , 0.5f);
    }
    public void OnBtnLoadScene(int index)
    {
        SceneManager.LoadScene(index);
        AudioManager.Instance.PlaySound(AudioManager.AudioName.ButtonClicked);


    }

    public void QuitGame()
    {
        Application.Quit();
    }
}