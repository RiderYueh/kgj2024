﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New LevelData", menuName = "ScriptableObject/LevelData")]
public class LevelData : ScriptableObject
{
    public List<LevelInfo> Levels = new List<LevelInfo>();
}