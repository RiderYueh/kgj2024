using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteManager : MonoBehaviour
{
    public static SpriteManager Instance = null;

    public List<Sprite> CardSprites = new();
    public List<Sprite> list_faceSprites;
    public List<Sprite> list_customerIcon;

    private void Awake()
    {
        Instance = this;
    }

    public Sprite GetSprite(int id)
    {
        if(id >= 0 && id < CardSprites.Count)
        {
            return CardSprites[id];
        }

        return null;
    }
}
