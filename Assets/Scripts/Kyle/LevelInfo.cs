﻿using System.Collections.Generic;

[System.Serializable]
public class LevelInfo
{
    public int TargetMoney;
    public int GuestAmount;
    public List<int> GuestType;
    public bool IsUseRandRule;
    public List<LevelRandRule> RuleList;
    public List<int> EachGuestCount;
}

[System.Serializable]
public class LevelRandRule
{
    public int GuestType;
    public bool IsFix;
    public int AmountMin;
    public int AmountMax;
}