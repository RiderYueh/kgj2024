﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="New GuestTypeData",menuName="ScriptableObject/GuestTypeData")]
public class GuestTypeData : ScriptableObject {
    public List<GuestInfo> Guests = new List<GuestInfo>();
}