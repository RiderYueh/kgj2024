﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(#TargetScriptName#))]
[CanEditMultipleObjects]
public class #SCRIPTNAME# : Editor {
    #TargetScriptName# mTarget;

    private void OnEnable(){
        mTarget = target as #TargetScriptName#;
    }

    public override void OnInspectorGUI(){
        DrawDefaultInspector();
        serializedObject.Update();
        serializedObject.ApplyModifiedProperties();
    }
}