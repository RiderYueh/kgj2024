using CliffLeeCL;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Food : MonoBehaviour
{
    public int CardID;
    public int CardType;
    public Image MainImage;
    public Text text_cost;
    public Text text_sell;

    public void Init(int cardID , int cardType)
    {
        CardID = cardID;
        CardType = cardType;

        text_cost.text = GameFlowManager.Instancce.GetPriceData.CardPriceList[cardType].Cost.ToString();
        text_sell.text = GameFlowManager.Instancce.GetPriceData.CardPriceList[cardType].Money.ToString();
    }

    public void OnClickFood()
    {
        
        if (!CustomerManager.instance.IsCanUseItem()) return;

        if (CardType == 0 || CardType == 1 || CardType == 2 || CardType == 3)
        {
            AudioManager.Instance.PlaySound(AudioManager.AudioName.FoodClicked);
        }
        else if (CardType == 4 || CardType == 5 || CardType == 6 || CardType == 7)
        {
            AudioManager.Instance.PlaySound(AudioManager.AudioName.AlcoholClicked);
        }
        else if (CardType == 8 || CardType == 9)
        {
            AudioManager.Instance.PlaySound(AudioManager.AudioName.DrinkClicked);
        }

        CardTest.instance.MainCard.OnPlayCardByID(CardID);
        FoodUI.instance.OnClickFood(CardID);
        GameFlowManager.Instancce.UseCard(CardType);

    }
}
